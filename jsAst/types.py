import json

class Node:
    def __init__(self,left=None,right=None,op=None) -> None:
        self.left = left
        self.right = right
        self.op = op

    def __str__(self) -> str:
        return '%s left:%s right:%s'%(self.op,json.dumps(self.left),json.dumps(self.right))

class Data:
    pass

class jInt(Data):
    def __init__(self,value=None) -> None:
        self.value=int(value)

    def __str__(self) -> str:
        return str(self.value)

class jFloat(Data):
    def __init__(self,value=None) -> None:
        self.value=float(value)

    def __str__(self) -> str:
        return str(self.value)

class jArray(Data):
    def __init__(self,value=None) -> None:
        self.value=value

    def __str__(self) -> str:
        return json.dumps(self.value)

class jObj(Data):
    def __init__(self,value=None) -> None:
        self.value=value

    def __str__(self) -> str:
        return json.dumps(self.value)

class jString(Data):
    def __init__(self,value=None) -> None:
        self.value=value

    def __str__(self) -> str:
        return self.value

class jVar(Data):
    def __init__(self,value=None) -> None:
        self.value=value

    def __str__(self) -> str:
        return f'var {self.value}'