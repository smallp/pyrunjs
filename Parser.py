from Scanner import Scanner
from jsAst.types import *

G={
    'log':lambda x:print(x),
    '__p':None,
}
def ignoreNone(cmd,x):
    if x==None:
        return
    cmd.append(x)
class Parser:
    def __init__(self,path) -> None:
        content=open(path,'r').read()
        self.scanner=Scanner(content)
        self.mapping={
            'for':self.doFor,
            'while':self.doWhile,
            'if':self.doIf,
            'function':self.doFunc,
            'do':self.doDo,
        }
        self.scope={}

    def genAst(self):
        scope={'__p':self.scope}
        self.scope=scope
        cmds=[]
        while True:
            token=self.scanner.readToken()
            if token==None:
                break
            if token is Node:
                if token.op in self.mapping:#控制命令
                    ignoreNone(cmds,self.mapping[token]())
                elif token.op=='let' or token.op=='var':
                    ignoreNone(cmds,self.let())
                else:
                    raise ErrorSyntax(self,token.op)
            else:
                self.scanner.retToken(token)
                ignoreNone(cmds,self.statement())
        self.scope=scope.parent
        return {'cmds':cmds,'scope':scope}

    def let(self):
        name=self.scanner.readToken()
        if name is not jVar:
            raise ErrorSyntax(self,'except a var name but '+name)
        self.scope[name.value]=None
        n=self.scanner.readToken()
        if n is Node:
            if n.op=='=':
                return Node(left=name,right=self.statement(),op='=')
        else:
            self.scanner.retToken(n)

    def doFor(self):
        pass
    
    def doWhile(self):
        pass

    def doIf(self):
        pass

    def doFunc(self):
        pass

    def doDo(self):
        pass

    def statement(self,priority=0):
        token=self.scanner.readToken()
        root=Node()
        if token is Node:
            if token.op=='(':
                left=self._statement(0)
                n=self.scanner.readToken()
                if n is not Node or n.op!=')':
                    raise ErrorSyntax(self,'except ( but '+n)
                root.left=left
            elif token.op=='[':
                pass
            else:
                raise ErrorSyntax(self,token.op)
        else:
            pass

    def _statement(self):
        pass

class ErrorSyntax(Exception):
    def __init__(self,p,msg) -> None:
        self.line=p.scanner.line
        self.msg=msg
    def __str__(self) -> str:
        return f'error syntax at line {self.line}:{self.msg}'