#  这是什么
这是用 python 写的一个 js 解释器，仅仅只是玩具。 只支持基本语法（for if do while），可以使用函数，但是不支持闭包。

#  如何使用
```bash
python3 main.py test.js
```
