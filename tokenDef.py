TOKEN = {
    'ADD': '+',
    'SUB': '-',
    'MUL': '*',
    'POW': '**',
    'DIV': '/',

    'QST': "?",
    'COL': ":",
    'LCMT': "/*",
    'RCMT': "*/",
    'LINE_CMT': "//",

    'GT': ">",
    'GE': ">=",
    'LT': "<",
    'LE': "<=",
    'EQ': "==",
    'NEQ': "!=",
    'NOT': "!",

    'AND': "&&",
    'OR': "||",
    'SEMI': ",",
    'COMMA': ";",

    'ASSIGN': "=",

    'VAR': "var",
    'IDENT': "identifier",

    'LPT': "(",
    'RPT': ")",
    'LBR': "{",
    'RBR': "}",
    'LMBR': "[",
    'RMBR': "]",
}
OPT_TOKEN=['do','if','while','for','function','return','var','let']
OPT_PRIORITY={
    '!':1,
    '.':1,
    '&&':1,
    '||':1,
    '>':4,
    '<':4,
    '>=':4,
    '<=':4,
    '==':4,
    '!=':4,
    '+':5,
    '-':5,
    '**':6,
    '*':10,
    '/':10,
    '.':20,
    '[':20,
    '(':20,
}